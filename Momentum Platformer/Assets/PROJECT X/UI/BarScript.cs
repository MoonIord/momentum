﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BarScript : MonoBehaviour {
	

	[SerializeField] private float lerpSpeed;
	[SerializeField] private Image filler;
	//[SerializeField] private Text valueText;
	private int momentumLevel = -1;

	private float fillAmount;

	public float maxValue { get; set; }

	public float Value{
		set {
			// Used for showing the value of the bar
			//string[] tmp = valueText.text.Split (':');
			//valueText.text = tmp [0] + ": " + value;
			fillAmount = Map(value, 0, maxValue, 0, 1);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		HandleBar ();
	}

	private void HandleBar () {
		if (fillAmount != filler.fillAmount) {
			filler.fillAmount = Mathf.Lerp(filler.fillAmount,fillAmount,Time.deltaTime * lerpSpeed);
		}

		if (momentumLevel == 0) {
			filler.color = new Color (1f, .392f, 0f, 1f);
		}
		else if (momentumLevel == 1) {
			filler.color = Color.red;
		}
		else if (momentumLevel == 2) {
			filler.color = Color.magenta;
		}
	}

	// Scales the value of the bar
	private float Map(float x, float min_i, float max_i, float min_f, float max_f) {
		return (x - min_i) * (max_f - min_f) / (max_i - min_i) + min_f;
	}

	public void SetMomentumLevelBar (int level) {
		momentumLevel = level;
	}

	public int GetMomentumLevelBar () {
		return momentumLevel;
	}
}
