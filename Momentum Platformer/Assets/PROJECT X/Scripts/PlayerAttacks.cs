﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAttacks{

	List<List<string>> attacksList = new List<List<string>>();


	// Use this for initialization
	void Awake() {
		attacksList.Add (new List<string>() {"DOWN", "UP", "DOWN"});
		attacksList.Add (new List<string>() {"LEFT","RIGHT","LEFT"});
	}

	public List<List<string>> GetAttacksInput() {
		Debug.Log (attacksList);
		return attacksList;
	}

	public string GetAttacksName() {
		// Should return the name that corresponds to the input
		return "none";
	}
		
}
