﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class PlayerStats{
	
	[SerializeField] private BarScript bar;
	[SerializeField] private float maxVal;
	[SerializeField] private float currentVal;

	public float CurrentVal {
		get {
			return currentVal;
		}
		set {
			this.currentVal = Mathf.Clamp(value,0,maxVal);
			bar.Value = currentVal;
		}
	}
	
	public float MaxVal {
		get {
			return maxVal;
		}
		set {
			this.maxVal = value;
			bar.maxValue = maxVal;
		}
	}

	public void Initialize() {
		this.MaxVal = maxVal;
		this.CurrentVal = currentVal;
	}

	public void SetMomentumLevelStat (int level) {
		bar.SetMomentumLevelBar(level);
	}

	public int GetMomentumLevelStat () {
		return bar.GetMomentumLevelBar ();
	}
}
