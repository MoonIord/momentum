﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour {

	// Stats like health, momentum. Remember to initialize these!
	[SerializeField] protected PlayerStats momentum;
	[SerializeField] protected PlayerStats health;
	protected bool canChangeMomentum = true;

	// Motion values
	[HideInInspector] public bool facingRight = true;
	[SerializeField] protected float moveSpeed = 5f;
	[SerializeField] protected float maxSpeed = 5f;
	protected bool doubleJump = false; 

	// Classes for animation, rigidbody and sprite
	protected Animator anim;
	protected Rigidbody2D rb2d;
	protected SpriteRenderer spriteRenderer;
	protected Color spriteColor;

	// Stops character from performing actions
	protected bool canMove = true;
	protected bool canAttack = true;

	// NEW GROUND CHECK AND JUMPING
	[SerializeField] protected Transform[] groundPoints;   // These are the GroundPoint (1) (2) on the Player
	[SerializeField] protected float groundRadius;
	[SerializeField] protected LayerMask whatIsGround;
	[SerializeField] protected LayerMask whatIsWall;

	[SerializeField] protected float minJumpForce;
	[SerializeField] protected float maxJumpForce;
	[HideInInspector] public bool jump = false;
	protected bool isGrounded;

	// TURNING PLAYER ON SLOPES
	public Quaternion currentRotation;
	protected Vector2 slopeAngle;
	// Sliding down slopes!
	protected bool canSlide;

	// WALL GRABBING AND JUMPING
	protected bool isWallTouch = false;
	protected bool wallJump = false;
	[SerializeField] protected Transform[] wallGrabPoints;  // WallGrabPoint (1) on the Player

	// PROJECTILES
	[SerializeField] protected GameObject shurikenPrefab;
	[SerializeField ]protected float throwTimer;
	[SerializeField] protected float throwCooldown;
	protected bool canThrow = true;

	// DODGING, PARRYING, BLOCKING
	protected bool invulnerable = false;
	[SerializeField] protected float invulnerableTime;
	protected bool dodging;
	protected float dodgingTime;
	[SerializeField] protected float maxDodgingTime;
	protected bool blocking = false;
	[SerializeField] protected float blockingTime;

	// Audio things
	public AudioSource playerAudio;
	public AudioClip deathClip;

	// Death and respawning
	protected bool isDead;
	protected Vector3 respawnPoint;


	/*
	 * Awake: Setup the component you are on right now ("this" object)
	 * Start: Setup the things that depend on other components
	 * 
	 */

	// Use this for initialization
	public virtual void Start () {
		anim = GetComponent<Animator>();
		rb2d = GetComponent<Rigidbody2D>();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		spriteColor = spriteRenderer.color;
		playerAudio = GetComponent<AudioSource>();
		momentum.Initialize ();
		health.Initialize ();
		respawnPoint = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

	}

	// OLD VERSION
	// Checks if player is grounded
	/*
	protected bool IsGrounded() {
		if (rb2d.velocity.y <= 0) {
			foreach (Transform point in groundPoints) {
				Collider2D[] colliders = Physics2D.OverlapCircleAll (point.position, groundRadius, whatIsGround);
				// Checks if the collider is different than the player
				for (int i = 0; i < colliders.Length; i++) {
					if(colliders[i].gameObject != gameObject) {
						return true;
					}
				}

			}
		}
		// If no collider is touching the ground
		return false;
	}
	*/

	// MY VERSION OF GROUNDED
	protected bool IsGrounded () {
		if (rb2d.velocity.y <= 0.1f) {
			RaycastHit2D hit = Physics2D.Raycast (transform.localPosition, transform.TransformDirection(Vector2.down),1.5f,whatIsGround);
			if (hit.collider != null) {
				return true;
			}
		}
		return false;
	}

	// Rotates player if on a slope
	protected void IsOnSlope () {
		currentRotation = Quaternion.Euler (transform.rotation.eulerAngles.x,
			transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
		RaycastHit2D hit = Physics2D.Raycast (transform.localPosition, transform.TransformDirection(Vector2.down),1.5f,whatIsGround);
		Debug.DrawRay (transform.localPosition, transform.TransformDirection (Vector2.down),Color.red,1f);
		slopeAngle = hit.normal;
		if (hit.collider != null) {
			rb2d.freezeRotation = false;
			float zrotation = hit.transform.rotation.eulerAngles.z;
			transform.localEulerAngles = (new Vector3 (0f, 0f, zrotation));
		} 
		else {
			// Stops the turning
			transform.localEulerAngles = (new Vector3 (0f, 0f, 0f));
			rb2d.freezeRotation = true;
		}
	}

	// Handles jumping
	protected virtual void Jump () {
		if ((isGrounded || doubleJump || isWallTouch) && jump) {

			if (isGrounded && jump) {
				isGrounded = false;
				doubleJump = true;
				rb2d.AddForce (new Vector2 (slopeAngle.x * maxJumpForce, slopeAngle.y * maxJumpForce));
			}
			else if (isWallTouch && !isGrounded) {
				isWallTouch = false;
				rb2d.AddForce (new Vector2 (0, maxJumpForce));
			}
			else if (doubleJump) {
				doubleJump = false;
				rb2d.AddForce (new Vector2 (0, maxJumpForce));
			}
		}
	}

	public void ChangeDirection () {
		facingRight = !facingRight;
		transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
	}

	public virtual void throwProjectile (int value) {
		// Eventually add parameter to generalize object being thrown

		// ADD COOLDOWN FOR THROWING

		if (facingRight && canThrow) {
			GameObject tmp = (GameObject) Instantiate(shurikenPrefab,transform.position + (Vector3.right * .95f),Quaternion.identity);
			tmp.GetComponent<ShurikenScript>().Initialize(Vector2.right);
		}
		else if (!facingRight && canThrow){
			GameObject tmp = (GameObject)  Instantiate(shurikenPrefab,transform.position + (Vector3.left * .95f),Quaternion.identity);
			tmp.GetComponent<ShurikenScript>().Initialize(Vector2.left);
		}
	}


	public virtual IEnumerator TakeDamage (float damage, float stunDuration) {
		// Should make the object invulnerable for invulDuration based on attack hit
		if (!invulnerable) {
			health.CurrentVal -= damage;

			// add if statement for !death
			invulnerable = true;
			StartCoroutine (IndicateInvulnerable());
			StartCoroutine (MoveWait (stunDuration));
			yield return new WaitForSeconds (invulnerableTime);
			invulnerable = false;
		}
	}

	protected IEnumerator IndicateInvulnerable() {
		while (invulnerable) {
			spriteRenderer.enabled = false;
			yield return new WaitForSeconds (.1f);
			spriteRenderer.enabled = true;
			yield return new WaitForSeconds (.1f);
		}
	}

	protected virtual void SlideDownSlope () {
		// If the slope is slanted to the right
		if (slopeAngle.x > 0 && Vector2.Angle (slopeAngle, Vector2.left) <= 45) {
			Debug.Log ("Sliding down left angled slope!");
			rb2d.AddForce( new Vector2 (-15f, -10f));
		} 
		else if (slopeAngle.x < 0 && Vector2.Angle (slopeAngle, Vector2.right) <= 45) {
			Debug.Log ("Sliding down right angled slope!");
			rb2d.AddForce ( new Vector2 (-15f, -10f));
		}
	}

	protected void IsDead () {
		if (health.CurrentVal <= 0 && !isDead) {
			isDead = true;
			invulnerable = true;
			canMove = false;
			spriteRenderer.color = Color.black;
			StartCoroutine (Respawn ());
		}
	}

	protected IEnumerator Respawn () {
		yield return new WaitForSeconds (2);
		spriteRenderer.color = spriteColor;
		transform.position = respawnPoint;
		health.CurrentVal = health.MaxVal;
		isDead = false;
		invulnerable = false;
		canMove = true;
	}

	protected IEnumerator AttackWait(float waitTime) {
		canAttack = false;
		yield return new WaitForSeconds (waitTime);
		canAttack = true;
	}

	protected IEnumerator MoveWait(float waitTime) {
		canMove = false;
		yield return new WaitForSeconds (waitTime);
		canMove = true;
	}
}
