﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackQueue : MonoBehaviour {

	// MAYBE DONT DO PRESET QUEUE HERE, ITD BE CREATED EACH TIME YOU WANT TO QUEUE AN ATTACK
	// AND THAT SEEMS RESOURCE INTENSIVE
	List<string> presetQueue;
	private bool keepChecking;

	// Use this for initialization
	void Initialize() {
	}

	// Allows the player one second to input directions
	private IEnumerator timeCheck () {
		keepChecking = true;
		yield return new WaitForSeconds (1);
		keepChecking = false;
	}

	public void ReadQueue () { //  add in ", List<string> presetQueue) {" when giving the list to compare it to
		// Should return string with v
		List<string> tmp = new List<string>();
		StartCoroutine (timeCheck());
		while (keepChecking) {
			// Else if statements limit combinations of input (ie. no UP=LEFT)

			if (Input.GetKeyDown(KeyCode.RightArrow)) {
				tmp.Add("RIGHT");
			}
			else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
				tmp.Add ("LEFT");
			}
			else if (Input.GetKeyDown(KeyCode.UpArrow)) {
				tmp.Add ("UP");
			}
			else if (Input.GetKeyDown(KeyCode.DownArrow)) {
				tmp.Add ("DOWN");
			} 
		}

		presetQueue = tmp;

	}

	public List<string> ReturnQueue () {
		return presetQueue;
	}
}
