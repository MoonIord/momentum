﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody2D))]
	
public class ShurikenScript : MonoBehaviour {


	[SerializeField] private float speed;
	private Rigidbody2D rb2d;
	private Vector2 direction;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate() {
		rb2d.velocity = direction * speed;
	}
	// Update is called once per frame
	void Update () {
	
	}

	// Can be accessed from player
	public void Initialize (Vector2 direction) {
		this.direction = direction;
	}

	// Destroys if off screen, might delete later
	void OnBecameInvisible () {
		Destroy (gameObject);
	}



}
