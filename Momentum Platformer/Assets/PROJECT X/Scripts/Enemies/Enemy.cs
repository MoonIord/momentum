﻿using UnityEngine;
using System.Collections;

public class Enemy : Character {

	// Enemy basic movement
	[SerializeField] private float patrolDuration;
	private float patrolTimer;
	private float direction = 1f;
	[SerializeField] private LayerMask whatIsPlayer;
	[SerializeField] private GameObject shieldPrefab;

	// Update is called once per frame
	void Update () {
		if (canMove && !isDead) {
			Patrol ();
		}
		IsDead ();
	}

	// Basic movement
	private void Patrol () {
		patrolTimer += Time.deltaTime;

		if (!FoundPlayer ()) {
			if (patrolTimer <= patrolDuration) {
				rb2d.AddForce (new Vector2 (5f * direction, 0f));
			} else {
				direction *= -1f;
				ChangeDirection ();
				patrolTimer = 0;
			}
		}
		else if (FoundPlayer()){
			if (canAttack) {
				AttackWait (.5f);
				AttackPlayer ();
			}
		}

	}

	// Raycasts to find nearby players, returns true if close enough to attack
	private bool FoundPlayer () {
		RaycastHit2D hit = Physics2D.Raycast (transform.localPosition, transform.TransformDirection(Vector2.right * direction),10f,whatIsPlayer);
		Debug.DrawRay (transform.localPosition, transform.TransformDirection (Vector2.right * direction * 10f));
		if (hit.collider != null) {
			// If less than 3 units away
			if (Mathf.Abs( hit.transform.position.x - transform.position.x) <= 3f) {
				return true;
			}
			else {
				float step = moveSpeed * Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, hit.transform.position, step);
				return false;
			}
		} else {
			return false;
		}
	}

	// ADD PARAMETERS FOR OBJECTPREFAB AND OBJECTSCRIPT
	// THE TIMER WILL BE FIXED WHEN THE ANIMATIONS COME OUT
	// DONT START A COROUTINE, WILL JUST QUEUE A WHOLE BUNCH OF PROJECTILES (I think)
	public override void throwProjectile (int value) {
		// Eventually add parameter to generalize object being thrown

		// ADD COOLDOWN FOR THROWING

		if (facingRight && canThrow) {
			GameObject tmp = (GameObject) Instantiate(shieldPrefab,transform.position + (Vector3.right * .85f),Quaternion.identity);
			tmp.GetComponent<ShieldScript>().Initialize(Vector2.right);
		}
		else if (!facingRight && canThrow){
			GameObject tmp = (GameObject)  Instantiate(shieldPrefab,transform.position + (Vector3.left * .85f),Quaternion.identity);
			tmp.GetComponent < ShieldScript>().Initialize(Vector2.left);
		}
	}

	private void OnTriggerEnter2D (Collider2D col) {

		if (col.gameObject.tag == "Projectile") {
			rb2d.AddForce(new Vector2 (0,200f));
			Destroy (col.gameObject);
			StartCoroutine(TakeDamage(20,.5f));
		}
	}

	private void AttackPlayer () {
		AttackWait (.5f);
		throwProjectile (1);
	}
}
