﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerControl : Character {

	// Variable jump height
	private float currentJumpTimer = 0f;
	[SerializeField] private float maxJumpTimer = .125f;
	private float myJumpForce;
	private bool increasingJump;

	// Queue attacks!
	[SerializeField] private AttackQueue attackQueue;
	private float attackTimer;


	void Awake () {
		health.CurrentVal = health.MaxVal;
		momentum.SetMomentumLevelStat (0);
		attackQueue = GetComponent<AttackQueue> ();
	}

	// Update is called once per frame
	void Update ()
	{
		float horizontal = Input.GetAxis("Horizontal");

        // These are jumps, dodges, etc.
        HandleInput();

        HandleAttacks ();

		// Rotates player if on a slope
		IsOnSlope();

		// Checks for death
		IsDead();

		// Used to transition between animations, try to figure out animator instead (ideally)
		//HandleAnimations (horizontal,isGrounded);

	}

	void FixedUpdate()
	{
        // Controls movement
        float horizontal = Input.GetAxis("Horizontal");

		isGrounded = IsGrounded();

        // Checks if grabbing wall
        isWallTouch = IsWallTouch(horizontal);

        // Movement
        if (canMove) {
			HandleMovement (horizontal);
			IsMoving ();
			if (Mathf.Abs (rb2d.velocity.x) > maxSpeed) {
				rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
			}

			// Lowers momentum from turning on the ground ******* no effect in air if add && isGrounded ********
			if (horizontal > 0 && !facingRight) {
				Flip ();
				momentum.CurrentVal *= .5f;
			}
			if (horizontal < 0 && facingRight) {
				Flip ();
				momentum.CurrentVal *= .5f;
			}
		}

	}


	// Handle animations
	private void HandleAnimations(float horizontal, bool isGrounded) {
		if (isGrounded && (Mathf.Abs (horizontal) < .01f)) {
			anim.SetInteger ("AnimState", 0);
		}
	}

	// Flips the player if turning directions
	void Flip()
	{
		ChangeDirection ();
	}

	private void HandleInput () {
		if (canMove) {

			VariableJump ();

			// TEST CODE FOR STATS BARS
			if (Input.GetKey (KeyCode.Q)) {
				health.CurrentVal -= 10;
			}
			if (Input.GetKey (KeyCode.W)) {
				health.CurrentVal += 10;
			}

			// DODGES!
			if (Input.GetKeyDown (KeyCode.R) && !dodging) {
				StartCoroutine (MoveWait (.25f));
				StartCoroutine (IndicateDodging ());
				rb2d.angularVelocity = 0f;
				rb2d.velocity = Vector2.zero;
				if (!isGrounded) {
					rb2d.AddForce (new Vector2 (0f, 50f));
				}
			}

			// Sliding down slopes
			if (Input.GetKey (KeyCode.DownArrow) && isGrounded) {
				SlideDownSlope ();
			}

			// TEST CODE FOR USING MOMENTUM
			if (Input.GetKeyDown (KeyCode.Z) && momentum.CurrentVal >= 50 && canChangeMomentum) {
				rb2d.AddForce (new Vector2 (0, -maxJumpForce));
				momentum.CurrentVal -= 50;
			}

			Jump ();
		}
	}

	private void HandleAttacks () {

		// Checks if throwing projectile
		if (Input.GetKeyDown(KeyCode.C) && canAttack) {
			StartCoroutine( AttackWait (.5f));
			throwProjectile (1);

		}

		if (Input.GetKeyDown (KeyCode.X) && canAttack) {
			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.LeftArrow)) {
				Debug.Log ("Attack side!");
				anim.SetInteger ("AnimState", 11);
				StartCoroutine( AttackWait (1f));
			} else if (Input.GetKey (KeyCode.UpArrow)) {
				Debug.Log ("Attack up!");
				StartCoroutine( AttackWait (3f));
			} else if (Input.GetKey (KeyCode.DownArrow)) {
				Debug.Log ("Attack down!");
				StartCoroutine( AttackWait (2f));
			} else {
				Debug.Log ("Attack neutral!");
				anim.SetInteger ("AnimState", 10);
				StartCoroutine( AttackWait (.25f));
			}
		}

		// Code for advancing momentum levels
		if (Input.GetKeyDown (KeyCode.E) && momentum.CurrentVal == 100) {
			momentum.SetMomentumLevelStat (momentum.GetMomentumLevelStat () + 1);
		} 
		else if (momentum.CurrentVal == 0 && momentum.GetMomentumLevelStat() > 0) {
			momentum.SetMomentumLevelStat (momentum.GetMomentumLevelStat () - 1);
			momentum.CurrentVal = 100;
			StartCoroutine(CanChangeMomentum ());
		}

		/*
		if (Input.GetKeyDown (KeyCode.X)) {
			Debug.Log ("Beginning queue...");
			attackQueue.ReadQueue ();
			Debug.Log ("Queue is: ");
		}
		*/
	}

	protected override void Jump () {
		if (jump && (isGrounded || doubleJump || isWallTouch)) {

			anim.SetInteger ("AnimState", 2);

			if (isGrounded) {
				isGrounded = false;
				doubleJump = true;
				rb2d.AddForce (new Vector2 (slopeAngle.x * myJumpForce, slopeAngle.y * myJumpForce));
				StartCoroutine(MoveWait (.1f));
                Debug.Log("Jumped! Double jump is: " + doubleJump);
			}
			else if (isWallTouch && !isGrounded) {
				isWallTouch = false;
				StartCoroutine(MoveWait (.35f));
				if (facingRight) {
					rb2d.AddForce (new Vector2 (-200f - (Mathf.Pow (momentum.CurrentVal, .5f) / 5f), maxJumpForce * 1.25f));
				} else if (!facingRight) {
					rb2d.AddForce (new Vector2 (200f + (Mathf.Pow (momentum.CurrentVal, .5f) / 5f), maxJumpForce * 1.25f));
				}
				Flip ();
			}
			else if (doubleJump) {
                Debug.Log("Is double jumping!");
				doubleJump = false;
				rb2d.AddForce (new Vector2 (0, maxJumpForce));
			}
			jump = false;
		}
	}

	private void VariableJump () {
        // Math for the variable jump
        if (isGrounded)
        {
            if (Input.GetKey(KeyCode.Space) && currentJumpTimer <= maxJumpTimer)
            {
                currentJumpTimer += Time.deltaTime;
                increasingJump = true;
            }
            else if (Input.GetKey(KeyCode.Space) && currentJumpTimer > maxJumpTimer)
            {
                myJumpForce = maxJumpForce;
            }
            else if (Input.GetKey(KeyCode.Space) != true && increasingJump)
            {
                increasingJump = false;
                myJumpForce = ((maxJumpForce - minJumpForce) * currentJumpTimer * 4f) + minJumpForce;
                jump = true;
                Debug.Log("My jump force is " + myJumpForce);
            }
            else
            {
                currentJumpTimer = 0;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Space)) { jump = true; } // For double jumps!
        else { jump = false; } // This is so jumps don't get queued in the air!
	}

	// Used to check if the player is moving at all to take away MOMENTUM
	private void IsMoving () {
		if ((Mathf.Abs(rb2d.velocity.x) < 1f) && (Mathf.Abs(rb2d.velocity.y) < 1f) && canChangeMomentum) {
			momentum.CurrentVal -= .5f;
		}
	}

	// Function in charge of moving player
	private void HandleMovement(float horizontal) {

		// MODIFY TO FIND NEW FORMULA FOR ACCELERATING
		rb2d.velocity = new Vector2 ((horizontal * (Mathf.Pow(momentum.CurrentVal + 1f,2)) * moveSpeed * .001f) + horizontal * moveSpeed, rb2d.velocity.y);

		// Adds momentum if moving
		if (rb2d.velocity.x != 0 && canChangeMomentum) {
			anim.SetInteger ("AnimState", 1);
			momentum.CurrentVal += 1f;
		}
	}

	// Checks if grabbing wall
    // CONSIDER ADDING TRIGGERS TO WALLS TO PREVENT FROM JUMPING ON SAME WALL ALL THE WAY UP, BUT ALLOW FOR WALL-JUMPING UP PARALLEL WALLS
	private bool IsWallTouch(float horizontal) {
		// Only works when moving cause of the horizontal float
		RaycastHit2D hit = Physics2D.Raycast (transform.localPosition, transform.TransformDirection(Vector2.right * horizontal),1f,whatIsWall);
		Debug.DrawRay (transform.localPosition, transform.TransformDirection (Vector2.right * horizontal));
		if (hit.collider != null) {
			return true;
		} else {
			return false;
		}
	}
		
	private void OnTriggerEnter2D (Collider2D col) {

		if (col.gameObject.tag == "Enemy Projectile") {
			Destroy (col.gameObject);
			StartCoroutine (TakeDamage (30,.25f));
		} else if (col.gameObject.tag == "Enemy") {
			rb2d.AddForce (new Vector2 (0f, 125f));
			StartCoroutine (TakeDamage (100,.5f));
		}
	}
		

	private IEnumerator IndicateDodging() {
		spriteRenderer.color = Color.red;
		invulnerable = true;
		while (dodgingTime <= maxDodgingTime) {
			dodgingTime += Time.timeScale;
			spriteRenderer.enabled = false;
			yield return new WaitForSeconds (.025f);
			spriteRenderer.enabled = true;
			yield return new WaitForSeconds (.05f);
		}
		invulnerable = false;
		dodgingTime = 0;
		spriteRenderer.color = spriteColor;
		dodging = false;
	}

	/*
	private IEnumerator IndicateParrying() {
		return true;
	}
	*/

	public override IEnumerator TakeDamage (float damage, float stunDuration) {
		// Should make the object invulnerable for invulDuration based on attack hit
		if (!invulnerable) {
			health.CurrentVal -= damage;

			// add if statement for !death
			invulnerable = true;
			StartCoroutine (IndicateInvulnerable());
			yield return new WaitForSeconds (invulnerableTime);
			invulnerable = false;
		}
	}

	private IEnumerator CanChangeMomentum () {
		canChangeMomentum = false;
		yield return new WaitForSeconds (1);
		canChangeMomentum = true;
	}

	protected override void SlideDownSlope () {
		// Debug.Log("Slope angle: " + Vector2.Angle (slopeAngle, Vector2.up));
		// Debug.Log ("x angle: " + slopeAngle.x);
		// If the slope is slanted to the right
		if (slopeAngle.x < 0 && Vector2.Angle (slopeAngle, Vector2.up) >= 20) {
			rb2d.velocity = new Vector2 (15f, -10f - (momentum.CurrentVal * .1f));
		} 
		else if (slopeAngle.x > 0 && Vector2.Angle (slopeAngle, Vector2.up) >= 20) {
			rb2d.velocity = new Vector2 (-15f, -10f - (momentum.CurrentVal * .1f));
		}
	}
		
}