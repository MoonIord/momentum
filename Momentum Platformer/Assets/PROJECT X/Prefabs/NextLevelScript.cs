﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class NextLevelScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggernEnter(Collider2D col) {
		if (col.gameObject.tag == "Player") {
			Debug.Log ("Help!");
			SceneManager.LoadScene ("NewWorld");
		}
	}
}
